				<section id="blog" class="section section-blog">
					<div class="animate-up">
                        <h2 class="section-title">From The Blog</h2>
                        
                        <!-- Boucle Foreach qui a pour but d'afficher les articles à la suite/à coté directement sous le "FROM THE BLOG", car avant ça n'affichait qu'un seul article puis "form the blog" puis le second article.
                        MAIS du coup le "FROM THE BLOG + les 2 articles se dupliquent... je ne sais pas comment régler ça -->
                        <?php foreach ($posts as $post ) : ?>
                        
                        <div class="blog-grid">
                            <div class="grid-sizer"></div>
                            <div class="grid-item">
                                <article class="post-box">
                                    <div class="post-data">
                                        <time class="post-datetime" datetime="<?php echo get_the_date() ?>">
                                            <!--<span class="day">03</span>
                                            <span class="month">MAY</span> -->
                                        </time>

                                        <div class="post-tag">
                                            <a href="index.php">#Photo</a>
                                            <a href="index.php">#Architect</a>
                                        </div>

                                        <h3 class="post-title">
                                            <!-- Ajout du lien permalink dans l'atribut title -->
                                            <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                                        </h3>

                                        <div class="post-info">
                                            <a href="index.php"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                            <a href="index.php"><i class="rsicon rsicon-comments"></i>0 comments</a>
                                        </div>
                                        <!-- Affichage des images de l'article si il y en a -->
                                        <?php 
                                            if ( has_post_thumbnail() )
                                                the_post_thumbnail('thumbnail');
                                            ?>
                                            <!-- Affichage d'un petit bout de l'article, pour initier un clic utilisateur -->
                                            <?php the_excerpt() ?>
                                    </div>
								</article>
                            </div>
                        </div>
                        <?php endforeach ?>
					</div>	
				</section><!-- #blog -->