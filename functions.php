<?php 
add_action( 'wp_enqueue_scripts', 'custom_scripts');
function custom_scripts() {
    wp_enqueue_style ('blog', get_template_directory_uri() . '/blog.css');
    wp_enqueue_style( 'font-fredokaone', 'https://fonts.googleapis.com/css?family=Fredoka+One');
    wp_enqueue_style( 'font-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic');

}

add_action( 'after_setup_theme', 'custom_theme_setup' ); function custom_theme_setup() {
    // WordPress Titles
     add_theme_support( 'title-tag' );
     add_theme_support( 'post-thumbnails' ); 
 
     // Menu du site
     register_nav_menus( array( 'primary' => 'Menu principal',
 ) );
 }
 
 // Ajouter un nouveau type de confenu sur le panneau admin de WP
 add_action( 'init', 'register_movie_post_type' );

 function register_movie_post_type() { 
     register_post_type( 'ArticleBonus', array(
         'labels' => array(
             'name' => __( 'ArticleBonus' ), 
             'singular_name' => __( 'ArticleBonus' ),
     ),
     'public' => true, 
     'has_archive' => true, 
     'supports' => array(
         'title', 
         'editor', 
         'thumbnail',
     ) 
     ) );
 }

?>