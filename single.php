<!-- Fonction qui appelle le Header de header.php -->
<?php get_header(); ?>

    <?php if (have_posts()) : while (have_posts() ) : the_post(); ?>
        <!-- Affichage des articles -->
        <?php get_template_part('content-single'); ?>
    <?php endwhile; endif; ?>
    </div><!-- .container -->
    </div><!-- .content -->
                
 <!-- Fonction qui appelle le Footer de footer.php -->
 <?php get_footer(); ?>